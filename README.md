# Queprec

![queprec in action](README/queprec.GIF)

## About
Queprec is a really simple SDL2 aplication to show inputs from any 12 button
gamepad in the style of a SNES controller under a GNU+Linux environment.

This can be useful for speedrunners or competitive players to show the input
to viewers or to record it to review later.

## Compile
You need the SDL2 development libraries to compile this code, on Debian official
sources the package is named `libsdl2-dev`.

After that, you can use the `make` tool to compile it with g++17, it will create
a binary named "joy" that you can launch with or without the `-c` or `--chroma`
flag. If you want to launch it without the chroma flag, be sure to have a compositer
to handle window transparency like compton or compiz.

**TLDR:**
```bash
sudo apt install libsdl2-dev
git clone https://gitlab.com/vum/queprec
cd queprec
make
./joy
```

## Contribute
Please feel free to fork this repository, I made this once because I needed it for
something and I don't think I will be active to accept pull requests.