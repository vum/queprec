/*

Copyright 2018, Facundo Lander <facundo@lander.cat>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include <fcntl.h>
#include <unistd.h>
#include <poll.h>

#include <stdio.h>
#include <string.h>

#include <map>
#include <algorithm>

#include <SDL2/SDL.h>
#include <iostream>

// SNES
#define N_BUTTONS 12

class button {
	public:
		SDL_Surface *image;
		SDL_Rect pos;
		void set(int x, int y, std::string image) {
			this->pos.x = x;
			this->pos.y = y;
			this->image = SDL_LoadBMP(image.c_str());
			if (!this->image) {
				printf("ERRRRRR\n");
			}
		}
		~button() {
			SDL_FreeSurface(this->image);
		}
};

class controller {

	private:
		int map[N_BUTTONS];
		int button[N_BUTTONS];

	public:
		controller() {
			this->map[0] = 256; // X
			this->map[1] = 257; // A
			this->map[2] = 258; // B
			this->map[3] = 259; // Y
			this->map[4] = 260; // L
			this->map[5] = 261; // R
			this->map[6] = 264; // Select
			this->map[7] = 265; // Start
			this->map[8] = 512; // Left
			this->map[9] = 512; // Right
			this->map[10] = 513; // Up
			this->map[11] = 513; // Down

			this->button[0] = false; // X
			this->button[1] = false; // A
			this->button[2] = false; // B
			this->button[3] = false; // Y
			this->button[4] = false; // L
			this->button[5] = false; // R
			this->button[6] = false; // Select
			this->button[7] = false; // Start
			this->button[8] = false; // Left
			this->button[9] = false; // Right
			this->button[10] = false; // Up
			this->button[11] = false; // Down
		}

		void setButtonState(int code, int value, bool state) {
			int index = 1337;
			for (int i = 0; i < N_BUTTONS; i++) {
				if (this->map[i] == code) {
					index = i;
					break;
				}
			}
			if (index == 1337) {
				return;
			}
			if (value != 0 && value < 127+N_BUTTONS) {
				index += value - 127;
			}
			this->button[index] = state;
			if (!state && (index == 10 || index == 8) ) {
				this->button[index+1] = false;
			}

			if (state) {
				switch (index) {
					case 8:
						this->button[9] = false;
						break;
					case 9:
						this->button[8] = false;
						break;
					case 10:
						this->button[11] = false;
						break;
					case 11:
						this->button[10] = false;
						break;
				}
			}
		}

		void showButtons() {
			for (int i = 0; i < N_BUTTONS; i++) {
				printf(this->button[i] ? "1" : "0");
			}
			printf("\n");
		}

		bool get(int b) {
			return this->button[b];
		}

};

int main(int argc, char *argv[]) {

	SDL_Init(SDL_INIT_VIDEO);
	SDL_Window *window;
	SDL_Surface *image;

	bool chroma = argc > 1 && (strcmp(argv[1], "-c") == 0 || strcmp(argv[1], "--chroma") == 0);

	if (chroma) {
		window = SDL_CreateWindow("Queprec", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 600, 300, 0);
		image = SDL_LoadBMP("coses/control_chroma.bmp");
	} else {
		window = SDL_CreateShapedWindow("Queprec", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 600, 300, 0);
		image = SDL_LoadBMP("coses/control.bmp");
	}

	SDL_Surface *screen = SDL_GetWindowSurface(window);
	SDL_WindowShapeMode mode;

	if (!chroma) {
		mode.mode = ShapeModeBinarizeAlpha;
		mode.parameters.binarizationCutoff = 255;
		SDL_SetWindowShape(window, image, &mode);
	}

	int timeout_ms = 10;
	char input_dev[] = "/dev/input/js1"; // Això del hardcoding és penós hehehe
	int st;
	int ret;
	struct pollfd fds[1];

	controller c;
	button buttons[N_BUTTONS];

	// Tots els botons, les seves coords i la seva imatge
	buttons[0].set(444,  90,  "coses/x.bmp");
	buttons[1].set(498,  132, "coses/a.bmp");
	buttons[2].set(444,  174, "coses/b.bmp");
	buttons[3].set(390,  132, "coses/y.bmp");
	buttons[4].set(78,   27,  "coses/sl.bmp");
	buttons[5].set(397,  26,  "coses/sr.bmp");
	buttons[6].set(228,  153, "coses/s.bmp");
	buttons[7].set(289,  153, "coses/s.bmp");
	buttons[8].set(153,  138, "coses/r.bmp");
	buttons[9].set(90,   138, "coses/l.bmp");
	buttons[10].set(121, 167, "coses/d.bmp");
	buttons[11].set(121, 106, "coses/u.bmp");

	fds[0].fd = open(input_dev, O_RDONLY|O_NONBLOCK);

	if (fds[0].fd < 0) {
		printf("No puc obrir '%s', està el control connectat?\n", input_dev);
		return(0);
	}

	const int input_size = 4096;
	unsigned char input_data[input_size];
	memset(input_data,0,input_size);

	fds[0].events = POLLIN;

	SDL_Event e;

	while (1) {

		SDL_BlitSurface(image, NULL, screen, NULL);

		ret = poll(fds, 1, timeout_ms);
		if (ret > 0) {
			if (fds[0].revents) {

				ssize_t r = read(fds[0].fd, input_data, input_size);

				for (int button = 0; button < r; button += 8) {

					int byte = button+4;
					//std::string action = (input_data[byte] == 0 ? "up" : "down");

					bool state =  input_data[byte] == 0 ? false : true;
					unsigned int button_value = input_data[byte+1];
					unsigned int button_code = (input_data[byte+2] << 8) + input_data[byte+3];

					c.setButtonState(button_code, button_value, state);
					c.showButtons();

				}
				memset(input_data, 0, input_size);
			}
		}

		// ESC per sortir
		SDL_PollEvent(&e);
		if ((e.type == SDL_KEYUP && e.key.keysym.scancode == SDL_SCANCODE_ESCAPE) || (e.type == SDL_QUIT)) {
			break;
		}

		for (int i = 0; i < N_BUTTONS; i++) {
			if (c.get(i)) {
				SDL_BlitSurface(buttons[i].image, NULL, screen, &buttons[i].pos);
			}
		}
		SDL_UpdateWindowSurface(window);
	}
	close(fds[0].fd);
	SDL_FreeSurface(image);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}